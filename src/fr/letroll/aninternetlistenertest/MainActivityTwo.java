package fr.letroll.aninternetlistenertest;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import fr.letroll.aninternetlistener.InternetListenerActivty;

public class MainActivityTwo extends InternetListenerActivty {

	private TextView tv;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main2);
		tv = (TextView) findViewById(R.id.textviewStatus);
	}

	@Override
	public void onConnexion(String type) {
		tv.setText(type+" connected");
	}
	
	@Override
	public void onConnexionChange(String type) {
		tv.setText(type);
	}

	@Override
	public void onConnexionShutdown() {
		tv.setText("shutdown");
	}

	public void scan(View v) {
		// tv.setText("signal: "+getWifiSignalStrength(this));
	}

}
