package fr.letroll.aninternetlistenertest;

import fr.letroll.aninternetlistener.ConnexionListener;
import fr.letroll.aninternetlistener.NetworkStateReceiver;
import android.app.Activity;
import android.content.IntentFilter;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends Activity implements ConnexionListener {

	private TextView tv;
	
	private IntentFilter filter;
	private NetworkStateReceiver receiver;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		tv= (TextView) findViewById(R.id.textviewStatus);
		
		receiver = new NetworkStateReceiver(this);
		filter=NetworkStateReceiver.getFilter();
	}

	@Override
	protected void onPause() {
		unregisterReceiver(receiver);
	    super.onPause();
	}
	
	@Override
	protected void onResume() {
		registerReceiver(receiver, filter);
	    super.onResume();
	}

	@Override
    public void onConnexionChange(String type) {
	    tv.setText(type);
    }

	@Override
    public void onConnexionShutdown() {
	    tv.setText("shutdown");
    }

	@Override
    public void onConnexion(String type) {
		tv.setText(type+" connected");
    }
}
